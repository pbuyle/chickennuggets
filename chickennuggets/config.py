from starlette.config import Config
from starlette.datastructures import URL

config = Config(".env")

SFTP_URL: URL = config("SFTP_URL", cast=URL, default="sftp://localhost:22")
KNOWN_HOSTS: str = config(
    "SFTP_KNOWN_HOSTS", default=f"{config('HOME')}/.ssh/known_hosts"
)

if SFTP_URL.scheme != "sftp":
    raise ValueError(f"Config 'SFTP_URL' has value '{SFTP_URL}'. Not a valid sftp url.")
