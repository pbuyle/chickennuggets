import asyncio
import base64
import mimetypes
import socket
import typing
import errno
from contextlib import asynccontextmanager
from email.utils import formatdate
from pathlib import PurePosixPath
from logging import getLogger

import asyncssh
import filetype
from calmsize import size as fmt_size
from starlette.datastructures import URL
from starlette.endpoints import HTTPEndpoint
from starlette.requests import Request
from starlette.responses import StreamingResponse, Response
from starlette.types import Scope, Receive, Send, ASGIApp

from chickennuggets.exceptions import HTTPException
from chickennuggets.templating import Jinja2Templates

templates = Jinja2Templates()

"""The maximum size (in byte) of a file header needed to guess its mime type"""
MAX_FILE_HEADER = 261


class Credentials(typing.NamedTuple):
    username: str
    password: str


AUTHORIZATION = "Authorization".lower().encode("latin-1")


class UsageCounter:
    def __init__(self):
        self._value = 0

    @property
    def value(self):
        return self._value

    def __enter__(self):
        self._value += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._value -= 1


def sftp_to_web(
    url: URL, known_hosts: typing.Optional[str] = None, client_factory: typing.Callable[[], asyncssh.SSHClient] = None,
) -> ASGIApp:
    """
    Return an ASGI application which provide an HTTP interface to a SFTP server.

    :param url: The URL of the sftp server, something like sftp://awesome.sauce:42
    :param known_hosts: Known SSH host, used to validate the SFTP server's key. This can be the name of a file
    containing a list of known hosts or a byte string containing a list of known hosts.
    :param client_factory: Function to create an instance of :class:`asyncssh.SSHClient` or a subclass.
    """

    def get_credentials(scope: Scope) -> Credentials:
        """
        Returns the SFTP credential from a ASGI HTTP scope
        :param scope:
        :return:
        """
        headers: typing.List[typing.Tuple[bytes, bytes]] = scope.get("headers", [])
        for header_key, header_value in headers:
            if header_key == AUTHORIZATION:
                scheme, credentials = header_value.decode("latin-1").split(maxsplit=1)
                if scheme.lower() == "basic":
                    return Credentials(*base64.b64decode(credentials).decode("ascii").split(":", maxsplit=1))
        raise HTTPException(status_code=401, headers={"WWW-Authenticate": f'Basic realm="{url}", charset="UTF-8"'})

    class Sftp2Web(HTTPEndpoint):
        def __init__(self, scope: Scope, receive: Receive, send: Send):
            super().__init__(scope, receive, send)
            self.credentials = get_credentials(scope)
            """The credential to use to connect to the SFTP server."""
            getLogger(__name__).debug(Credentials(self.credentials.username, "********"))

        __connections: typing.Dict[
            Credentials,
            typing.Tuple[
                typing.Awaitable[typing.Tuple[asyncssh.SSHClientConnection, asyncssh.SFTPClient]],
                typing.Optional[asyncio.Task],
                UsageCounter,
            ],
        ] = {}
        """
        Dictionary of cached SSH connections and their SFTP clients indexed by credentials.
        Each entry is a tuple composed of
          - an awaitable for the connection and SFTP client
          - an optional timer task which will eventually close the client and connection
          - an usage counter

        """

        __connections_ttl = 300
        """Time to live of cached SSH connections and their SFTP clients"""

        async def _sftp_connect(self) -> typing.Tuple[asyncssh.SSHClientConnection, asyncssh.SFTPClient]:
            """
            Open a SSH connection and start a SFTP client.
            """
            conn = await asyncssh.connect(
                host=url.hostname or "localhost",
                port=url.port or 22,
                username=self.credentials.username,
                password=self.credentials.password,
                client_factory=client_factory,
                known_hosts=known_hosts,
            )
            conn.set_keepalive(60)
            sftp = await conn.start_sftp_client()
            return conn, sftp

        @asynccontextmanager
        async def sftp(self) -> asyncssh.SFTPClient:
            """
            Provides a SFTP client as runtime context.

            The SSH connection and SFTP client are automatically open and closed.
            Connection and client are cached in memory for re-use.
            """
            cached: typing.Awaitable[typing.Tuple[asyncssh.SSHClientConnection, asyncssh.SFTPClient]]
            cached, timer, counter = self.__connections.get(self.credentials, (None, None, UsageCounter()))
            if timer is not None:
                # Cancel closing the existing connection and client.
                timer.cancel()

            if cached is None:
                cached = asyncio.create_task(self._sftp_connect())
                self.__connections[self.credentials] = (cached, None, counter)

            conn, sftp = await asyncio.shield(cached)
            try:
                with counter:
                    yield sftp
            finally:
                if counter.value == 0:

                    async def close():
                        await asyncio.sleep(self.__connections_ttl)
                        del self.__connections[self.credentials]
                        sftp.exit()
                        await sftp.wait_closed()
                        conn.close()
                        await conn.wait_closed()

                    self.__connections[self.credentials] = (
                        cached,
                        asyncio.create_task(close()),
                        counter,
                    )

        async def dispatch(self) -> None:
            """
            Dispatch this Endpoint request to the handler method.

            This extend the base class `dispatch` method for error handling.

            :return:
            """
            try:
                await super().dispatch()
            except asyncssh.SFTPError as err:
                if err.code in sftp_error_mapper:
                    raise sftp_error_mapper[err.code](err)
                raise err
            except asyncssh.Error as err:
                if err.code in ssh_error_mapper:
                    raise ssh_error_mapper[err.code](err)
                raise err
            except (socket.gaierror, socket.herror) as err:
                raise HTTPException(status_code=502, detail=err.strerror)
            except OSError as err:
                if err.errno in os_error_mapper:
                    raise os_error_mapper[err.errno](err)
                raise err
            except ConnectionError as err:
                raise HTTPException(status_code=502, detail=err.strerror)

        async def __get_file(self, request: Request) -> typing.AsyncGenerator[bytes, None]:
            """
            Returns a generator for the content of the response to a GET request for a file.
            """
            async with self.sftp() as sftp:
                async with sftp.open(request.url.path, encoding=None) as file:
                    while data := await file.read():
                        yield data

        async def __get_dir_html(self, request: Request) -> typing.AsyncGenerator[str, None]:
            """
            Returns a generator for the content of the response to a GET request for a directory.
            """

            async def read_dir(path):
                class FileInfo(typing.TypedDict):
                    filename: str
                    filepath: PurePosixPath
                    size: str
                    hsize: str
                    mtime: str
                    type: str
                    icon: str

                async with self.sftp() as sftp:

                    async def file_info(filename: asyncssh.SFTPName) -> FileInfo:
                        filepath = f"{path}/{filename.filename}"
                        if await sftp.isdir(filepath):
                            type = "directory"
                            icon = "📁"
                        else:
                            type = "file"
                            icon = "📄"

                        return {
                            "filename": filename.filename,
                            "filepath": PurePosixPath(request.url.path, filename.filename),
                            "size": f"{filename.attrs.size:,}",
                            "hsize": fmt_size(filename.attrs.size),
                            "mtime": formatdate(filename.attrs.mtime),
                            "type": type,
                            "icon": icon,
                        }

                    if path != "/":
                        parent_path = PurePosixPath(request.url.path).parent
                        stat = await sftp.stat(parent_path)
                        yield {
                            "filename": "Parent Directory",
                            "filepath": parent_path,
                            "size": f"{stat.size:,}",
                            "hsize": fmt_size(stat.size),
                            "mtime": formatdate(stat.mtime),
                            "type": "directory",
                            "icon": "↩️",
                        }

                    pending: typing.List[typing.Awaitable[FileInfo]] = [
                        file_info(x)
                        for x in sorted(await sftp.readdir(path), key=lambda f: f.filename)
                        if x.filename[0] != "."
                    ]

                    c = len(pending)
                    # Use a queue to limit concurrency
                    inqueue = asyncio.Queue(maxsize=5)

                    async def enqueue():
                        for f in pending:
                            await inqueue.put(asyncio.create_task(f))

                    asyncio.create_task(enqueue())

                    while c > 0:
                        yield await (await inqueue.get())
                        c -= 1

            return templates.get_template("index.html").generate_async(
                {"request": request, "files": read_dir(request.url.path), "cwd": request.url.path,}
            )

        async def get(self, request: Request) -> Response:
            async with self.sftp() as sftp:
                if not await sftp.exists(request.url.path):
                    raise HTTPException(status_code=404)
                if await sftp.isfile(request.url.path):
                    file_path = PurePosixPath(request.url.path)
                    headers = await self.__get_http_headers(sftp, file_path)
                    if request.headers.get("if-none-match") == headers.get("etag"):
                        return NotModifiedResponse(headers=headers)
                    else:
                        return StreamingResponse(
                            content=self.__get_file(request), media_type=headers.get("content-type"), headers=headers,
                        )

                if await sftp.isdir(request.url.path):
                    await sftp.stat(request.url.path)  # Trigger access control check which need to be done early.
                    return StreamingResponse(content=await self.__get_dir_html(request), media_type="text/html")

                # The requested resource is not a file or directory
                raise HTTPException(status_code=501)

        @classmethod
        async def __get_http_headers(cls, sftp: asyncssh.SFTPClient, path: PurePosixPath) -> typing.Dict[str, str]:
            """
            Return the HTTP headers for remote file at the given path

            :param sftp: The client to use to read to remote file
            :param path: The path of the remote file
            :return: A dictionary of strings
            """
            async with sftp.open(path, encoding=None) as file:
                (media_type, etag, attrs) = await asyncio.gather(
                    cls.__get_content_type(path, file), cls.__get_etag(path, file), file.stat(),
                )
                return {
                    "content-type": media_type,
                    "etag": etag,
                    "vary": "Authorization",
                    "cache-control": "private",
                    "content-length": str(attrs.size),
                    "last-modified": formatdate(attrs.mtime),
                }

        @classmethod
        async def __get_etag(cls, path: PurePosixPath, file: asyncssh.SFTPClientFile) -> str:
            """
            Compute the Etag for the remote file at the given path.

            :param path: The path of the remote file
            :param file: A remote file open to read data as bytes
            :return: The etag of the remote file as a string
            """
            attrs = await file.stat()
            # The etag is the hash of the concatenation of the path, the size, the owner id and group id, the last
            # modification time, and the first 2k of the file.
            etag = (
                attrs.size.to_bytes(8, "big"),
                attrs.uid.to_bytes(4, "big"),
                attrs.gid.to_bytes(4, "big"),
                attrs.mtime.to_bytes(4, "big"),
                await cls.__get_file_head(file),
            )
            return f'W/"{hex(hash(etag))[2:-1]}"'

        @classmethod
        async def __get_content_type(cls, path: PurePosixPath, file: asyncssh.SFTPClientFile) -> str:
            """
            Get the content type of the remote file at the given path.

            :param path: The path of the remote file
            :param file: A remote file open to read data as `bytes`
            :return: The content type of the remote file as a `str`
            """
            try:
                head = await cls.__get_file_head(file)
                return filetype.guess_mime(head) or mimetypes.guess_type(path)[0] or "application/octets-stream"
            except asyncssh.SFTPError:
                return mimetypes.guess_type(path)[0] or "application/octets-stream"

        @classmethod
        async def __get_file_head(cls, file: asyncssh.SFTPClientFile) -> bytes:
            """
            Retrieve the file header needed to determine its mime type.

            This method will read up to MAX_FILE_HEADER from the remote file and return them as `bytes`
            :param file: A remote file open to read data as `bytes`
            :return: The header of the file.
            """
            head: bytes = b""
            while len(head) < MAX_FILE_HEADER:
                data = await file.read(MAX_FILE_HEADER - len(head))
                if not data:
                    break
            return head

    return Sftp2Web


class NotModifiedResponse(Response):
    NOT_MODIFIED_HEADERS = (
        "cache-control",
        "content-location",
        "date",
        "etag",
        "expires",
        "vary",
    )

    def __init__(self, headers: dict):
        super().__init__(
            status_code=304,
            headers={name: value for name, value in headers.items() if name in self.NOT_MODIFIED_HEADERS},
        )


def mk_http_exception(status_code: int, detail: str):
    def _f(err):
        return HTTPException(status_code=status_code, detail=getattr(err, detail, detail))

    return _f


sftp_error_mapper: typing.Dict[int, typing.Callable] = {
    asyncssh.FX_NO_SUCH_FILE: mk_http_exception(status_code=404, detail="reason"),
    asyncssh.FX_PERMISSION_DENIED: mk_http_exception(status_code=403, detail="reason"),
}

ssh_error_mapper: typing.Dict[int, typing.Callable] = {
    asyncssh.DISC_NO_MORE_AUTH_METHODS_AVAILABLE: mk_http_exception(status_code=403, detail="reason")
}

os_error_mapper: typing.Dict[int, typing.Callable] = {
    errno.ENETUNREACH: mk_http_exception(status_code=502, detail="strerror"),
    errno.ENETRESET: mk_http_exception(status_code=502, detail="strerror"),
    errno.ETIMEDOUT: mk_http_exception(status_code=502, detail="strerror"),
    errno.ECONNREFUSED: mk_http_exception(status_code=502, detail="strerror"),
    errno.EHOSTDOWN: mk_http_exception(status_code=502, detail="strerror"),
    errno.EHOSTUNREACH: mk_http_exception(status_code=502, detail="strerror"),
    errno.ECONNRESET: mk_http_exception(status_code=502, detail="strerror"),
}
