from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.requests import Request
from starlette.responses import Response, PlainTextResponse


class HTTPException(StarletteHTTPException):
    """
    Extends Starlettes's HTTPException class with optional headers.
    """

    def __init__(
        self, status_code: int, detail: str = None, headers: dict = None
    ) -> None:
        super().__init__(status_code, detail)
        self.headers = headers

    def __repr__(self) -> str:
        class_name = self.__class__.__name__
        if self.headers is None:
            return f"{class_name}(status_code={self.status_code!r}, detail={self.detail!r}, headers={self.headers})"
        return f"{class_name}(status_code={self.status_code!r}, detail={self.detail!r})"


def http_exception(request: Request, exc: HTTPException) -> Response:
    headers = getattr(exc, "headers", None)
    if exc.status_code in {204, 304}:
        return Response(b"", status_code=exc.status_code, headers=headers)
    return PlainTextResponse(exc.detail, status_code=exc.status_code, headers=headers)


exception_handlers = {StarletteHTTPException: http_exception}
