import typing

import jinja2
import starlette.templating


class Jinja2Templates(starlette.templating.Jinja2Templates):
    """
    Like starlette.templating.Jinja2Templates but with async support and package loader.
    """

    def __init__(self) -> None:
        assert jinja2 is not None, "jinja2 must be installed to use Jinja2Templates"
        self.env = self.get_env()

    def get_env(self) -> "jinja2.Environment":
        @jinja2.contextfunction
        def url_for(context: dict, name: str, **path_params: typing.Any) -> str:
            request = context["request"]
            return request.url_for(name, **path_params)

        loader = jinja2.PackageLoader("chickennuggets", "templates")
        env = jinja2.Environment(loader=loader, autoescape=True, enable_async=True)
        env.globals["url_for"] = url_for
        return env
