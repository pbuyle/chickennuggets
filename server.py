from starlette.applications import Starlette
from starlette.routing import Mount

from chickennuggets import config
from chickennuggets.exceptions import exception_handlers
from chickennuggets.app import sftp_to_web

app = Starlette(
    routes=[Mount("/", sftp_to_web(config.SFTP_URL, config.KNOWN_HOSTS))], exception_handlers=exception_handlers,
)
