Chicken Nuggets
===============

A simple minimalist read-only HTTP gateway for SFTP.

⚠️ Chicken Nuggets is a work in progress. Use at your own risk.

The server is an ASGI_ application. It can be run in any ASGI server.
For instance, to run it with Uvicorn_:

::

  $ uvicorn server:app

Configuration is done with the following environment variables:

- ``SFTP_URL``: URL of the SFTP server to expose on web (eg. ``sftp://awesome.sauce:42``). Username and password form the URL are ignored. Defaults to ``sftp://localhost:22``
- ``KNOWN_HOSTS`` File path to the SSH known hosts file. Defaults to ``$HOME//.ssh/known_hosts``. The file must contains the SSH public key for the SFTP server.

⚠️  Chicken Nuggets does not do HTTPS. It is designed to be to be behind a reverse proxy which would use HTTPS. Do not expose it directly on the internet without one.

.. _ASGI: https://asgi.readthedocs.io/en/latest/
.. _Uvicorn: https://www.uvicorn.org/