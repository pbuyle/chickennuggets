import base64
import unittest
from pathlib import Path
from email.utils import formatdate
import asyncssh
import pytest
from async_asgi_testclient import TestClient
from starlette.datastructures import URL

from chickennuggets.exceptions import HTTPException
from chickennuggets.app import sftp_to_web


class TestSSHServer(asyncssh.SSHServer):
    def begin_auth(self, username):
        return True

    def password_auth_supported(self):
        return True

    def validate_password(self, username, password):
        return username == password


class SSHClient(asyncssh.SSHClient):
    def validate_host_public_key(self, host, addr, port, key):
        return True


def client_factory() -> asyncssh.SSHClient:
    return SSHClient()


class TestSftp2WebGet:
    @pytest.fixture
    def tmp_root(self, tmp_path: Path):
        return tmp_path

    @pytest.fixture
    async def sftp_server(self, unused_tcp_port, tmp_root: Path):
        server = await asyncssh.listen(
            host="",
            port=unused_tcp_port,
            server_host_keys=[asyncssh.generate_private_key("ssh-dss")],
            server_factory=TestSSHServer,
            sftp_factory=lambda chan: asyncssh.SFTPServer(chan=chan, chroot=str(tmp_root)),
        )
        yield {"port": unused_tcp_port}
        server.close()
        await server.wait_closed()

    @pytest.fixture
    def app(self, sftp_server):
        async def application(scope, receive, send):
            await sftp_to_web(URL(f"sftp://localhost:{sftp_server['port']}"), client_factory=client_factory,)(
                scope, receive, send
            )

        return application

    @pytest.fixture
    def client(self, app):
        return TestClient(app)

    @pytest.mark.asyncio
    async def test_requires_authorization_header(self, client):
        with pytest.raises(HTTPException) as cm:
            await client.get("/")
        assert cm.value.status_code == 401
        assert cm.value.headers is not None
        assert "WWW-Authenticate" in cm.value.headers

    @pytest.mark.asyncio
    async def test_wrong_credentials(self, client):
        with pytest.raises(HTTPException) as cm:
            await client.get(
                "/", headers={"Authorization": authorization_header("foo", "bar")},
            )
        assert cm.value.status_code == 403

    @pytest.mark.asyncio
    async def test_file_not_found(self, client):
        with pytest.raises(HTTPException) as cm:
            await client.get(
                "/foo.bar", headers={"Authorization": authorization_header("foo", "foo")},
            )

        assert cm.value.status_code == 404

    @pytest.mark.asyncio
    async def test_get_file(self, client, tmp_root):
        (tmp_root / "foo.bar").write_text("FOO BAR")
        response = await client.get("/foo.bar", headers={"Authorization": authorization_header("foo", "foo")},)
        assert response.status_code == 200
        assert response.text == "FOO BAR"
        # response should have a etag header whose value is a weak validator
        assert "etag" in response.headers
        assert response.headers["etag"][0:2] == "W/"

    @pytest.mark.asyncio
    async def test_get_dir(self, client, tmp_root, snapshot):
        (tmp_root / "dir").mkdir()
        now = formatdate()
        for n in range(0, 1000):
            (tmp_root / "dir" / f"file-{format(n, '04')}.txt").touch()

        response = await client.get("/dir", headers={"Authorization": authorization_header("foo", "foo")},)
        assert response.status_code == 200
        # FIXME: Find a better way to test the response content
        snapshot.assert_match(response.text.replace(now, "Sat, 06 Jun 2020 02:28:33 -0000"))


def authorization_header(username: str, password: str):
    credentials = f"{username}:{password}"
    return f"Basic {base64.b64encode(credentials.encode('ascii')).decode('ascii')}"


if __name__ == "__main__":
    unittest.main()
